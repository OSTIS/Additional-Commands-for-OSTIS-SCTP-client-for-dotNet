﻿using Ostis.Sctp.Arguments;
using Ostis.Sctp.Commands;
using Ostis.Sctp.Responses;
using System;
using System.Collections.Generic;

namespace Ostis.Sctp.CompositeCommands
{
    /// <summary>
    /// Статический класс, содержащий неатомарные команды для работы с OSTIS SCTP-клиентом для .NET.
    /// </summary>
    public static class Commands
    {
        #region Обработка SC-ссылок

        /// <summary>
        /// Создание ссылки с указанным значением.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="content">Значение SC-ссылки.</param>
        /// <returns>Пара ("результат установки значения", "SC-адрес созданной SC-ссылки"). Полностью успешно - (true, "валидный SC-адрес"). Ссылка создана, но значение не установлено - (false, "валидный SC-адрес). Иначе - (false, ScAddress.Invalid).</returns>
        public static Tuple<bool, ScAddress> CreateLinkWithContent(SctpClient client, LinkContent content)
        {
            if (!client.IsConnected) return Tuple.Create(false, ScAddress.Invalid);

            var cmdCreateLink = new CreateLinkCommand();
            var rspCreateLink = (CreateLinkResponse)client.Send(cmdCreateLink);
            var cmdSetValue = new SetLinkContentCommand(rspCreateLink.CreatedLinkAddress, content);
            var rspSetValue = (SetLinkContentResponse)client.Send(cmdSetValue);
            return Tuple.Create(rspSetValue.ContentIsSet, rspCreateLink.CreatedLinkAddress);
        }

        /// <summary>
        /// Поиск значения SC-ссылки по её SC-адресу.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="linkAddress">SC-адрес SC-ссылки.</param>
        /// <returns>Если поиск успешен - экземпляр LinkContent (не Invalid), иначе - LinkContent.Invalid.</returns>
        public static LinkContent FindLinkContent(SctpClient client, ScAddress linkAddress)
        {
            if (!client.IsConnected) return LinkContent.Invalid;

            var cmdGetLink = new GetLinkContentCommand(linkAddress);
            var rspGetLink = (GetLinkContentResponse)client.Send(cmdGetLink);

            return rspGetLink.Header.ReturnCode == ReturnCode.Successfull ?
                new LinkContent(rspGetLink.LinkContent) :
                LinkContent.Invalid;
        }


        /// <summary>
        /// Поиск cтрокового значения SC-ссылки по её SC-адресу.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="linkAddress">SC-адрес SC-ссылки.</param>
        /// <returns>Если поиск успешен - строка, иначе - null.</returns>
        public static string FindStringLinkContent(SctpClient client, ScAddress linkAddress)
        {
            if (!client.IsConnected) return null;

            var cmdGetLink = new GetLinkContentCommand(linkAddress);
            var rspGetLink = (GetLinkContentResponse)client.Send(cmdGetLink);
            return rspGetLink.Header.ReturnCode == ReturnCode.Successfull ?
                LinkContent.ToString(rspGetLink.LinkContent) :
                null;
        }

        /// <summary>
        /// Поиск значения SC-ссылки (LinkContent) по указанному неролевому отношению и связанному этим отношением узлу.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="linkedNodeAddr">SC-адрес связанного с искомой ссылкой узла.</param>
        /// <param name="relationNodeAddr">SC-адрес узла неролевого отношения.</param>
        /// <returns>Если поиск успешен - экземпляр LinkContent со значением, иначе - LinkContent.Invalid.</returns>
        public static LinkContent FindLinkContentByNoroleRelation(SctpClient client,
            ScAddress linkedNodeAddr, ScAddress relationNodeAddr)
        {
            //итерируем конструкцию
            var template = new ConstructionTemplate(linkedNodeAddr, ElementType.ConstantCommonArc_c, ElementType.Link_a, ElementType.PositiveConstantPermanentAccessArc_c, relationNodeAddr);
            var cmdIterateElements = new IterateElementsCommand(template);
            var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

            //если число конструкций равно 1, то ищем значение ссылки
            if (rspIterateElements.Constructions.Count != 1) return LinkContent.Invalid;

            var cmdGetValue = new GetLinkContentCommand(rspIterateElements.Constructions[0][2]);
            var rspGetValue = (GetLinkContentResponse)client.Send(cmdGetValue);
            return new LinkContent(rspGetValue.LinkContent);
        }

        #endregion Обработка SC-ссылок

        /// <summary>
        /// Получить коллекцию конструкций, соответствующих указанной конструкции-шаблону.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="template">Конструкция-шаблон.</param>
        /// <returns>В случае успешного поиска - непустая коллекция конструкций, иначе - null.</returns>
        public static List<List<ScAddress>> IterateElements(SctpClient client, ConstructionTemplate template)
        {
            if (!client.IsConnected) return null;
            var cmdIter = new IterateElementsCommand(template);
            var rspIter = (IterateElementsResponse)client.Send(cmdIter);
            return rspIter.Constructions;
        }

        /// <summary>
        /// Установка указанного системного идентификатора для существующего узла с указанным SC-адресом.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="nodeAddress">SC-адрес узла.</param>
        /// <param name="sysIdtf">Системный идентификатор.</param>
        /// <returns>Успешно - true, иначе false.</returns>
        public static bool SetSysIdentifier(SctpClient client, ScAddress nodeAddress, Identifier sysIdtf)
        {
            if (!client.IsConnected) return false;

            var cmdSetId = new SetSystemIdCommand(nodeAddress, sysIdtf);
            var rspSetId = (SetSystemIdResponse)client.Send(cmdSetId);

            return rspSetId.IsSuccesfull;
        }

        /// <summary>
        /// Создание узла указанного типа.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="nodeType">Тип узла.</param>
        /// <returns>Если успешно - валидный SC-адрес созданного узла, иначе - ScAddress.Invalid.</returns>
        public static ScAddress CreateNode(SctpClient client, ElementType nodeType)
        {
            if (!client.IsConnected) return ScAddress.Invalid;

            var cmdCreateNode = new CreateNodeCommand(nodeType);
            var rspCreateNode = (CreateNodeResponse)client.Send(cmdCreateNode);
            return rspCreateNode.CreatedNodeAddress;
        }

        /// <summary>
        /// Создание узла указанного типа c идентификатором.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="nodeType">Тип узла.</param>
        /// /// <param name="identifier">Системный идентификатор.</param>
        /// <returns>Если успешно - валидный SC-адрес созданного узла, иначе - ScAddress.Invalid.</returns>
        public static ScAddress CreateNode(SctpClient client, ElementType nodeType, Identifier identifier)
        {
            if (!client.IsConnected) return ScAddress.Invalid;

            var cmdCreateNode = new CreateNodeCommand(nodeType);
            var rspCreateNode = (CreateNodeResponse)client.Send(cmdCreateNode);

            ScAddress newNode = rspCreateNode.CreatedNodeAddress;
            return SetSysIdentifier(client, newNode, identifier) ? newNode :  ScAddress.Invalid;
        }

        /// <summary>
        /// Создание дуги указанного типа между двумя указанными узлами.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="arcType">Тип дуги.</param>
        /// <param name="beginElement">Узел-начало дуги.</param>
        /// <param name="endElement">Узел-конец дуги.</param>
        /// <returns>Если успешно - валидный SC-адрес созданной дуги, иначе - ScAddress.Invalid.</returns>
        public static ScAddress CreateArc(SctpClient client, ElementType arcType,
            ScAddress beginElement, ScAddress endElement)
        {
            if (!client.IsConnected) return ScAddress.Invalid;

            var cmdCreateArc = new CreateArcCommand(arcType, beginElement, endElement);
            var rspCreateArc = (CreateArcResponse)client.Send(cmdCreateArc);
            return rspCreateArc.CreatedArcAddress;
        }


        /// <summary>
        /// Создание дуги указанного типа между двумя указанными узлами.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="arcType">Тип дуги.</param>
        /// <param name="beginElement">Узел-начало дуги.</param>
        /// <param name="endElement">Узел-конец дуги.</param>
        /// /// <param name="predicateId">Идентификатор предиката.</param>
        /// <returns>Если успешно - валидный SC-адрес созданной дуги, иначе - ScAddress.Invalid.</returns>
        public static ScAddress CreateArc(SctpClient client, ElementType arcType,
            ScAddress beginElement, ScAddress endElement, Identifier predicateId)
        {
            if (!client.IsConnected) return ScAddress.Invalid;

            var cmdCreateArc = new CreateArcCommand(arcType, beginElement, endElement);
            var rspCreateArc = (CreateArcResponse)client.Send(cmdCreateArc);
            ScAddress arc =  rspCreateArc.CreatedArcAddress;

            ScAddress predicate = FindAddressByIdentifier(client, predicateId);

            cmdCreateArc = new CreateArcCommand(ElementType.PositiveConstantPermanentAccessArc_c, predicate, arc);
            rspCreateArc = (CreateArcResponse)client.Send(cmdCreateArc);

            return arc;
        }

        /// <summary>
        /// Удаление элемента по указанному SC-адресу.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="address">SC-адрес удаляемого элемента.</param>
        /// <returns></returns>
        public static bool DeleteElement(SctpClient client, ScAddress address)
        {
            if (!client.IsConnected) return false;

            var cmdDel = new DeleteElementCommand(address);
            var rspDel = (DeleteElementResponse)client.Send(cmdDel);
            return rspDel.IsDeleted;
        }

        /// <summary>
        /// Получить тип элемента по указанному SC-адресу.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="address">SC-адрес элемента.</param>
        /// <returns>Если успешно - известный тип элемента (не ElementType.Unknown), иначе - ElementType.Unknown.</returns>
        public static ElementType GetElementType(SctpClient client, ScAddress address)
        {
            if (!client.IsConnected) return ElementType.Unknown;

            var cmdGetType = new GetElementTypeCommand(address);
            var rspGetType = (GetElementTypeResponse)client.Send(cmdGetType);
            return rspGetType.Header.ReturnCode == ReturnCode.Successfull
                ? rspGetType.ElementType
                : ElementType.Unknown;
        }

        /// <summary>
        /// Поиск SC-адреса (ScAddress) элемента с указанным идентификатором (Identifier).
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="identifier">Идентификатор искомого элемента.</param>
        /// <returns>Если поиск успешен, то непосредственно найденный ScAddress узла; иначе - ScAddress.Invalid.</returns>
        public static ScAddress FindAddressByIdentifier(SctpClient client, Identifier identifier)
        {
            if (!client.IsConnected) return ScAddress.Invalid;

            var cmdFindElement = new FindElementCommand(identifier);
            var rspFindElement = (FindElementResponse)client.Send(cmdFindElement);

            return rspFindElement.Header.ReturnCode == ReturnCode.Successfull
                ? rspFindElement.FoundAddress
                : ScAddress.Invalid;
        }

        /// <summary>
        /// Поиск идентификатора (указанного типа) узла по SC-адресу узла.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="address">SC-адрес узла.</param>
        /// <param name="identifierType">Идентификатор типа идентификатора (например, системный).</param>
        /// <returns>Если поиск успешен, то непосредственно найденный Identifier узла; иначе - Identifier.Invalid.</returns>
        public static Identifier FindNodeIdentifierByAddress(SctpClient client,
            ScAddress address, Identifier identifierType)
        {
            if (!client.IsConnected) return Identifier.Invalid;

            var template = new ConstructionTemplate(address,
                ElementType.ConstantCommonArc_c,
                ElementType.Link_a,
                ElementType.PositiveConstantPermanentAccessArc_c,
                FindAddressByIdentifier(client, identifierType));
            var iterCmd = new IterateElementsCommand(template);
            var iterRsp = (IterateElementsResponse)client.Send(iterCmd);

            if (iterRsp.Constructions.Count != 1)
                return Identifier.Invalid;

            ScAddress link = iterRsp.Constructions[0][2];
            var cmdGetLink = new GetLinkContentCommand(link);
            var rspGetLink = (GetLinkContentResponse)client.Send(cmdGetLink);
            return rspGetLink.Header.ReturnCode == ReturnCode.Successfull
                ? LinkContent.ToString(rspGetLink.LinkContent)
                : Identifier.Invalid;
        }

        /// <summary>
        /// Поиск системного (уникального в рамках БЗ) идентификатора узла по SC-адресу узла.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="address">SC-адрес узла.</param>
        /// <returns>Если поиск успешен, то непосредственно найденный Identifier узла; иначе - Identifier.Invalid.</returns>
        public static Identifier FindNodeSysIdentifierByAddress(SctpClient client, ScAddress address)
        {
            return FindNodeIdentifierByAddress(client, address, "nrel_system_identifier");
        }

        /// <summary>
        /// Поиск главного русскоязычного идентификатора узла по SC-адресу узла.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="node">SC-адрес узла.</param>
        /// <returns>Если поиск успешен, то найденный адрес ссылки; иначе - ScAddress.Invalid.</returns>
        public static ScAddress FindNodeMainIdtf(SctpClient client, ScAddress node)
        {
            if (!client.IsConnected) return ScAddress.Invalid;

            ScAddress predicate = Commands.FindAddressByIdentifier(client, "nrel_main_idtf");

            var template = new ConstructionTemplate(node, ElementType.ConstantCommonArc_c,
                ElementType.Link_a, ElementType.PositiveConstantPermanentAccessArc_c, predicate);
            var cmdIterateElements = new IterateElementsCommand(template);
            var rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

            foreach (var constuction in rspIterateElements.Constructions)
            {
                ScAddress link = constuction[2];
                predicate = Commands.FindAddressByIdentifier(client, "lang_ru");

                template = new ConstructionTemplate(predicate, ElementType.PositiveConstantPermanentAccessArc_c, link);
                cmdIterateElements = new IterateElementsCommand(template);
                rspIterateElements = (IterateElementsResponse)client.Send(cmdIterateElements);

                if (rspIterateElements.Constructions.Count == 1)
                {
                    return link;
                }
            }
            return ScAddress.Invalid;
        }

        /// <summary>
        /// Поиск главного русскоязычного идентификатора узла по SC-адресу узла.
        /// </summary>
        /// <param name="client">Ссылка на экземпляр подключенного SCTP-клиента.</param>
        /// <param name="target">SC-адрес узла.</param>
        /// <param name="value">Строковое содержимое главного идентификатора</param>
        /// <returns>Если операция успешна, то true. Иначе, false.</returns>
        public static bool CreateNodeMainIdtf(SctpClient client, ScAddress target, string value)
        {
            if (!client.IsConnected) return false;

            LinkContent content = new LinkContent(value);
            Tuple<bool, ScAddress> response = CreateLinkWithContent(client, content);

            if(!response.Item1)
            {
                return false;
            }
      
            ScAddress link = response.Item2;

            ScAddress lang_node = FindAddressByIdentifier(client, "lang_ru");

            CreateArc(client, ElementType.ConstantCommonArc_c, target, link, "nrel_main_idtf");
            ScAddress arc = CreateArc(client, ElementType.PositiveConstantPermanentAccessArc_c, lang_node, link);

            if(arc == ScAddress.Invalid)
            {
                return false;
            }

            return true;
        }
    }
}